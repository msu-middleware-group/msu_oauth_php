<?php

use Symfony\Component\Yaml\Yaml;

group('install', function() {
  desc('Create the configuration file');
  task('config', function() {
    $clientId = readline('Client Application ID: ');
    $clientSecret = readline('Client Secret: ');
    $redirectUrl = readline('Redirect URL: ');

    createConfig($clientId, $clientSecret, $redirectUrl);

    echo "Configuration file created.\n";
  });

  desc('Setup the database');
  task('database', function() {
    if(file_exists('db/oauth_sample.db')) {
      echo "Database already exists...skipping setup.\n";
      return;
    }

    $pdo = new PDO('sqlite:db/oauth_sample.db');
    $pdo->query("CREATE TABLE users(
      uid               CHAR(50)      PRIMARY KEY                 NOT NULL,
      name              CHAR(30)                                  NOT NULL
    );");
    $pdo->query("CREATE TABLE session(
      sess_id           VARCHAR(255)  PRIMARY KEY                 NOT NULL,
      sess_data         TEXT                                      NOT NULL,
      sess_time         INT(11)                                   NOT NULL
    );");
    unset($pdo);
    chmod('db/', 0777);
    chmod('db/oauth_sample.db', 0666);

    echo "Database setup completed.\n";
  });
});

desc('Install the application');
task('install', 'install:config', 'install:database', function() {});

function createConfig($clientId, $clientSecret, $redirectUrl) {
  $config_contents = array(
    'authorize_url' => 'https://oauth.dev.ais.msu.edu/oauth/authorize',
    'token_url'     => 'https://oauth.dev.ais.msu.edu/oauth/token',
    'client_id'     => $clientId,
    'client_secret' => $clientSecret,
    'redirect_url'  => $redirectUrl
  );

  file_put_contents('./config/oauth.yaml', Yaml::dump($config_contents));
}
