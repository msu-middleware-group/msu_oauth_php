# msu\_oauth\_php

## Description

Sample application using Michigan State University's OAuth provider. **This
application is a work in progress.**

## Dependencies

* PHP >= 5.3
* Composer

## Installation

* ```git clone https://gitlab.msu.edu/msu-middleware-group/msu_oauth_php.git```
* ```composer install```
* ```./phake install```
  * Enter the appropriate details
* Access the application

