<?php
class User {
  protected $pdo = null;

  protected $uid = null;
  protected $name = null;

  public function __construct(PDO $pdo) {
    $this->pdo = $pdo;
  }

  public function save() {
    try {
      $sth = $this->pdo->prepare('INSERT INTO users (uid, name) VALUES(:uid, :name)');
      $sth->execute(array(
        ':uid'   => $this->getUid(),
        ':name'   => $this->getName()
      ));
    } catch (Exception $e) {
      throw new Exception('Duplicate uid error');
    }
  }

  public function destroy() {
    $sth = $this->pdo->prepare('DELETE FROM users WHERE uid = :uid');
    $sth->execute(array(
      ':uid'   => $this->getUid()
    ));
  }

  public function load($uid) {
    $sth = $this->pdo->prepare('SELECT name FROM users WHERE uid = :uid');
    $sth->execute(array(
      ':uid'   => $uid
    ));
    $record = $sth->fetch();

    if($record == '') {
      throw new Exception('Unknown uid error');
    }

    $this->uid = $uid;
    $this->name = $record['name'];
  }

  public function getName() {
    return $this->name;
  }

  public function setName($name) {
    $this->name = $name;
  }

  public function getUid() {
    return $this->uid;
  }

  public function setuid($uid) {
    $this->uid = $uid;
  }

  public function __toString() {
    return isset($this->name) ? $this->name : 'unknown';
  }
}

