<html>
  <head>
    <title>Sample PHP OAuth Application</title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />
  </head>
  <body>
    <header>
      <h1>MSU PHP OAuth2 Test</h1>
      <button onclick="parent.location='login.php'">Login</button>
    </header>

This is a sample landing page for the PHP test application for MSU's
OAuth2 service.  Click the login button to go some place.

  </body>
</html>
