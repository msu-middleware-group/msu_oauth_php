<html>
  <head>
    <title>Sample PHP OAuth Application</title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />
  </head>
  <body>
    <header>
      <h1>MSU PHP OAuth2 Test</h1>
    </header>
<?php

// Dependencies
require '../vendor/autoload.php';
use Symfony\Component\Yaml\Yaml;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\PdoSessionHandler;

require '../lib/User.php';

// database
$pdo = new PDO('sqlite:../db/oauth_sample.db');
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
// oauth settings
$settings = Yaml::parse(file_get_contents('../config/oauth.yaml'));

// oauth object
$msu = new MSU\OAuth2\Client(
  $settings['client_id'],
  $settings['client_secret'],
  $settings['redirect_url']
);

// session management
$storage = new NativeSessionStorage(
  array(),
  new PdoSessionHandler($pdo, array('db_table' => 'session'))
);
$session = new Session($storage);

if(isset($_GET['logout'])) {
  echo "You are logged out<br/>";
  echo 'Return to the <a href="/">landing page</a>.';
  $session->invalidate();
  exit;
}

if(isset($_GET['code'])) {
  $accessToken = $msu->getAccessToken($_GET['code']);
  $session->set('access_token', $accessToken);
}

$accessToken = $session->get('access_token');
if(!isset($accessToken)) {
  $msu->authenticate();
}

// user is authenticated.
$uid = $session->get('uid');
if(!isset($uid)) {
  $msu->setAccessToken($accessToken);
  $content = $msu->fetch();
  $uid = $content['uid'];
  $session->set('uid', $uid);
}

// this user was unregistered
$user = new User($pdo);
try {
  $user->load($uid);
} catch(Exception $e) {
  $user->setUid($uid);
  $user->setName($content['info']['name']);
  try {
    $user->save();
  } catch(Exception $e) {
    echo 'there was an error with your registration<br/>';
    exit();
  }
  echo 'you have been registered<br/>';
}

header('Location: user-page.php');
exit();
