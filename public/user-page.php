<html>
  <head>
    <title>Sample PHP OAuth Application</title>
    <link href="css/style.css" rel="stylesheet" type="text/css" />
  </head>
  <body>
    <header>
      <h1>MSU PHP OAuth2 Test</h1>
      <button onclick="parent.location='login.php?logout=logout'">Logout</button>
    </header>
<?php

// Dependencies
require '../vendor/autoload.php';
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Symfony\Component\HttpFoundation\Session\Storage\Handler\PdoSessionHandler;

require '../lib/User.php';


// database
$pdo = new PDO('sqlite:../db/oauth_sample.db');
$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

// session management
$storage = new NativeSessionStorage(
  array(),
  new PdoSessionHandler($pdo, array('db_table' => 'session'))
);
$session = new Session($storage);

$accessToken = $session->get('access_token');
if(!isset($accessToken)) {
  header('Location: login.php');
  exit();
}

$uid = $session->get('uid');

$user = new User($pdo);
$user->load($uid);

echo "You are logged in $user and are viewing a protected resource.<br/>";
echo '<a href="login.php?logout=logout">Logout</a>';

